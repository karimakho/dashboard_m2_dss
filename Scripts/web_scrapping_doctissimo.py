# Extraction script for Doctissimo PMA forum
# Start at 10:55 AM - 2024-06-04
import requests
from bs4 import BeautifulSoup
import pandas as pd
import time


def get_page_content(url):
    # Set up the headers to mimic a browser visit
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}

    # Send a GET request to the URL
    response = requests.get(url, headers=headers)

    # Check if the request was successful
    if response.status_code == 200:
        # Parse the page content with BeautifulSoup
        return BeautifulSoup(response.content, 'html.parser')
    else:
        print(f"Failed to retrieve the page. Status code: {response.status_code}")
        return None


def extract_post_data(soup):
    posts = []
    # Find all threads on the forum page
    threads = soup.find_all('tr', class_='sujet')
    print(f"Found {len(threads)} threads on the page.")

    # Iterate through each thread
    for thread in threads:
        post = {}

        # Extract the title and URL of the post
        title_element = thread.find('a', class_='cCatTopic')
        if title_element:
            post['title'] = title_element.get_text(strip=True)
            post_link = title_element['href']
            if post_link.startswith("/"):
                post['url'] = f"https://forum.doctissimo.fr{post_link}"
            else:
                post['url'] = post_link
            print(f"Post found: {post['title']} - URL: {post['url']}")
        else:
            post['title'] = 'No title'
            post['url'] = None
            print("No title found for a post.")

        # Extract the author of the post
        author_element = thread.find('td', class_='sujetCase6')
        post['author'] = author_element.get_text(strip=True) if author_element else 'Unknown'
        print(f"Author: {post['author']}")

        # Extract the number of replies
        replies_element = thread.find('td', class_='sujetCase7')
        post['replies'] = replies_element.get_text(strip=True) if replies_element else '0'
        print(f"Replies: {post['replies']}")

        # Extract the number of views
        views_element = thread.find('td', class_='sujetCase8')
        post['views'] = views_element.get_text(strip=True) if views_element else '0'
        print(f"Views: {post['views']}")

        # Extract the date of the last post
        last_post_element = thread.find('time')
        post['last_post_date'] = last_post_element['datetime'] if last_post_element else 'Unknown'
        print(f"Last post date: {post['last_post_date']}")

        posts.append(post)
    return posts


def extract_comments(post_url):
    comments = []
    # Get the content of the post page
    soup = get_page_content(post_url)
    if soup:
        # Find all comment blocks on the post page
        comment_blocks = soup.find_all('div', class_='md-post__content-body mdjs__see-more-container collapse')
        print(f"Found {len(comment_blocks)} comments on the page.")

        # Iterate through each comment block
        for block in comment_blocks:
            comment = {}
            # Extract the text of the comment
            comment['content'] = block.get_text(strip=True) if block else 'No content'
            # print(f"Comment content: {comment['content']}")
            comments.append(comment)
    return comments


def main():
    base_url = 'https://forum.doctissimo.fr/grossesse-bebe/fertilite-infertilite/liste_sujet-10.htm?page='
    all_posts = []

    # Iterate through each page of the forum
    for page in range(1, 162):  # Adjusted to cover pages from 1 to 162
        url = f"{base_url}{page}"
        # print(f"Scraping page: {url}")

        # Get the content of the forum page
        soup = get_page_content(url)
        if soup:
            # Extract post data from the page
            posts = extract_post_data(soup)
            for post in posts:
                if post['url']:
                    print(f"Extracting comments for post: {post['title']}")
                    # Extract comments for each post
                    post['comments'] = extract_comments(post['url'])
                    time.sleep(1)  # Sleep to avoid making too many requests too quickly
                else:
                    post['comments'] = []
                all_posts.append(post)
        else:
            print(f"Failed to retrieve content from {url}")
        time.sleep(2)  # Sleep to avoid making too many requests too quickly

    # Convert the list of posts to a DataFrame and save it to a CSV file
    df = pd.DataFrame(all_posts)
    df.to_csv('/Users/bazinenohaila/Desktop/pma_doctissimo_posts.csv', index=False, encoding='utf-8')
    print("Data has been saved to '/Users/bazinenohaila/Desktop/pma_doctissimo_posts.csv'")


if __name__ == "__main__":
    main()
